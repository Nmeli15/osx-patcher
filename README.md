# OS X Patcher
OS X Patcher is an [open source](#license) command line tool for running OS X Mountain Lion, OS X Mavericks, OS X Yosemite, and OS X El Capitan on unsupported Macs

## Linux makes Macs better
If you can, you shoud really consider [using Linux](https://julianfairfax.gitlab.io/blog/20221029/linux-makes-macs-better) instead of this patcher.

## Software Updates
When installing software updates on your unsupported Mac, this patcher's patches may be overwritten by the new versions of system files that are being installed. It's highly recommended to have a patched installer to patch your system after the update.

## Contributors
I'd like to the thank the following people, and many others, for their research, help, and inspiration.
- [andyvand](https://github.com/andyvand) for the efilipo tool (source code in [this folder](https://github.com/andyvand/macosxbootloader/tree/master/efilipo))
- [parrotgeek1](https://github.com/parrotgeek1) for the Mountain Lion Macmini1,1/2,1 AirPort patch, El Capitan SIPManager patch (source code in [the SIPManager folder](/SIPManager)), and for invaluable help in starting this project
- [TMRJIJ](https://github.com/TMRJIJ) for invaluable help in starting this project
- [Piker Alpha](https://github.com/Piker-Alpha) for the 32-bit boot.efi (source code in [this repo](https://github.com/Piker-Alpha/macosxbootloader))

## Supported Macs

### MacBooks
- MacBook2,1
- MacBook3,1
- MacBook4,1

### MacBook Airs
- MacBookAir1,1

### MacBook Pros
- MacBookPro2,1
- MacBookPro2,2

### iMacs
- iMac4,1
- iMac4,2
- iMac5,1
- iMac5,2
- iMac6,1

### Mac minis
- Macmini1,1
- Macmini2,1

### Mac Pros
- MacPro1,1
- MacPro2,1

### Xserves
- Xserve1,1
- Xserve2,1

## Supported Versions of OS X
- 10.8 Mountain Lion
- 10.9 Mavericks
- 10.10 Yosemite
- 10.11 El Capitan

## Known Issues
### Graphics Acceleration, Sleep, and Brightness Control

Your Mac won't support graphics acceleration with this patcher. Sleep and brightness control also won't work. And applications that rely on your graphics card will perform slower or will simply not work. If you have an AMD ATI card, sleep and brightness control will work fine. The system has been set to not attempt to sleep. If you would like to disable this, run sudo pmset -a disablesleep 0

Affected devices:
- All (sleep and brightness control unaffected on AMD ATI cards)

Affected versions of OS X:
- 10.8 Mountain Lion
- 10.9 Mavericks
- 10.10 Yosemite
- 10.11 El Capitan

### Wi-Fi Connectivity

If you have a Mac mini, your Wi-Fi card will not work on Mavericks and newer. This means you'll only able to connect to the internet through Ethernet or another connection.

Affected devices:
- Macmini1,1
- Macmini2,1

Affected versions of OS X:
- 10.9 Mavericks
- 10.10 Yosemite
- 10.11 El Capitan

## Usage

### Create Installer Drive

#### Step 1

Download the latest version of the patcher from the GitHub releases page.

You can also download it from the Homebrew repository that is now available [here](https://gitlab.com/julianfairfax/package-repo).

#### Step 2

Open Disk Utility and select your installer drive, then click Erase.

#### Step 3

Select Mac OS Extended Journaled and click Erase.

#### Step 4

Unzip the download and open Terminal. Type chmod +x and drag the script file to Terminal, then hit enter. Then type sudo, drag the script file to Terminal, and hit enter.

#### Step 5

Drag your installer app to Terminal. Then select your installer drive from the list and copy and paste the number.

### Erase System Drive (optional)

These Steps are optional. A clean install is recommended but not required. It's recommended to make a Time Machine backup before erasing your system drive.

#### Step 1

Open the Utilities menu and click Disk Utility. Select your system drive and click Erase.

#### Step 2

Select Mac OS Extended Journaled and click Erase.

### Install the OS

#### Step 1

Boot from your installer drive by holding down the option key when booting and selecting your installer drive from the menu. Then select your language from the list.

#### Step 2

Close Disk Utility if you used it to erase your system drive, then click Continue.

#### Step 3

Select your system drive, the drive to install the OS on, and click Continue.

Wait 35-45 minutes and click Reboot when the installation is complete. Then boot into your installer drive using the previous instructions.

### Patch the OS

This is the important part. This is the part where you install the patcher files onto your system. If you miss this part or forget it then your system won't boot.

#### Step 1

Open the Utilities menu and click Terminal. Type patch and hit enter. Make sure to select the model your drive will be used with. Then select your system drive from the list and copy and paste the number.

Wait 5-10 minutes and reboot when the patch tool has finished patching your system drive. Then boot into your system drive and setup the OS.

### Restore the OS

If you've switched to a new Mac or just want to remove the patcher files from your system, you can run the restore tool from your installer drive.

#### Step 1

Boot from your installer drive by holding down the option key when booting and selecting your installer drive from the menu. Then select your language from the list.

#### Step 2

Open the Utilities menu and click Terminal. Type restore and hit enter. Make sure to select the model you selected when you last patched. Then select your system drive from the list and copy and paste the number.

Wait 5-10 minutes, then reinstall the OS.

## Docs
If you want to know how OS X Patcher works, you can check [this doc](https://julianfairfax.github.io/docs/patching-macos-to-run-on-unsupported-macs.html) I wrote about patching macOS to run on unsupported Macs and [this one](https://julianfairfax.github.io/docs/patching-macos-to-install-on-unsupported-macs.html) about patching macOS to install on unsupported Macs. You can also review the code yourself and feel free to submit a pull request if you think part of it can be improved.

## License
The following files and folders were created by me and are licensed under the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/):
- OS X Patcher.sh
- resources/InstallerMenuAdditions.plist
- resources/patch.sh
- resources/restore.sh
- resources/patch/com.apple.PowerManagement.plist

The following files and folders were modified by me and those modificatins are licensed under the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/):
- resources/prelinkedkernel, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-install-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version) from Apple, from 10.11.6
- resources/patch/boot.efi, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-firmware-problem) from Piker Alpha and Apple, from 10.10
- resources/patch/IOUSBHostFamily.kext, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#what-new-patches-do-i-have-to-use-for-this-version-1) from Apple, from 10.10.5
- resources/patch/10.11/boot.efi, [modified](https://julianfairfax.gitlab.io/documentation/patching-macos-to-run-on-unsupported-macs#the-32-bit-firmware-problem-1) from Piker Alpha and Apple, from 10.11

The following files and folders were created by other developers and are licensed under their licenses:
- resources/patch/SIPManager.kext, compiled by me, from parrotgeek1
- SIPManager, by parrotgeek1

The following files and folders were created by Apple and are licensed under their licenses:
- resources/dm, from [10.7 recovery update](https://download.info.apple.com/Mac_OS_X/041-2768.20111012.cd14A/RecoveryHDUpdate.dmg)
- resources/patch/airportd, from 10.8.3
- resources/patch/AppleHDA.kext, from 10.7.5
- resources/patch/AppleHIDMouse.kext, from 10.10.5
- resources/patch/AppleIntelGMA950GA.plugin, from 10.6.2 beta 1
- resources/patch/AppleIntelGMA950GLDriver.bundle, from 10.6.2 beta 1
- resources/patch/AppleIntelGMA950.kext, from 10.6.2 beta 1
- resources/patch/AppleIntelGMA950VADriver.bundle, from 10.6.2 beta 1
- resources/patch/AppleIntelGMAX3100FB.kext, from 10.6.2 beta 1
- resources/patch/AppleIntelGMAX3100GA.plugin, from 10.6.2 beta 1
- resources/patch/AppleIntelGMAX3100GLDriver.bundle, from 10.6.2 beta 1
- resources/patch/AppleIntelGMAX3100.kext, from 10.6.2 beta 1
- resources/patch/AppleIntelGMAX3100VADriver.bundle, from 10.6.2 beta 1
- resources/patch/AppleIntelIntegratedFramebuffer.kext, from 10.6.2 beta 1
- resources/patch/AppleIRController.kext, from 10.7.5
- resources/patch/AppleTopCase.kext, from 10.10.5
- resources/patch/AppleUSBMultitouch.kext, from 10.10.5
- resources/patch/AppleUSBTopCase.kext, from 10.10.5
- resources/patch/ATI1300Controller.kext, from 10.7.5
- resources/patch/ATI1600Controller.kext, from 10.7.5
- resources/patch/ATI1900Controller.kext, from 10.7.5
- resources/patch/ATIFramebuffer.kext, from 10.7.5
- resources/patch/ATIRadeonX1000GA.plugin, from 10.7.5
- resources/patch/ATIRadeonX1000GLDriver.bundle, from 10.7.5
- resources/patch/ATIRadeonX1000.kext, from 10.7.5
- resources/patch/ATIRadeonX1000VADriver.bundle, from 10.7.5
- resources/patch/ATISupport.kext, from 10.7.5
- resources/patch/GeForce7xxxGLDriver.bundle, from 10.6.2 beta 1
- resources/patch/GeForce8xxxGLDriver.bundle, from 10.6.2 beta 1
- resources/patch/GeForceGA.plugin, from 10.6.2 beta 1
- resources/patch/GeForce.kext, from 10.6.2 beta 1
- resources/patch/GeForceVADriver.bundle, from 10.6.2 beta 1
- resources/patch/IO80211Family.kext, from 10.8 beta 1
- resources/patch/IOAudioFamily.kext, from 10.8.4
- resources/patch/IOBDStorageFamily.kext, from 10.10.5
- resources/patch/IOBluetoothFamily.kext, from 10.10.5
- resources/patch/IOBluetoothHIDDriver.kext, from 10.10.5
- resources/patch/IOSerialFamily.kext, from 10.10.5
- resources/patch/IOUSBFamily.kext, from 10.10.5
- resources/patch/IOUSBMassStorageClass.kext, from 10.10.5
- resources/patch/kernel, from 10.11.6 final
- resources/patch/NVDANV40Hal.kext, from 10.6.2 beta 1
- resources/patch/NVDANV50Hal.kext, from 10.6.2 beta 1
- resources/patch/NVDAResman.kext, from 10.6.2 beta 1
